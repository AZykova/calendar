import Document, { Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  static async getInitialProps(ctx) {
    const initialProps = await Document.getInitialProps(ctx);
    return { ...initialProps }
  }

  render() {
    return (
      <html>
        <Head>
          <meta charSet="utf-8" />
          <meta name="viewport" content="width=device-width,initial-scale=1,shrink-to-fit=no,viewport-fit=cover" />
          <style global jsx>{`
            @font-face {
              font-family: 'BeelineSans';
              font-weight: 400;
              font-style: normal;
              font-display: swap;
              src: local('BeelineSans'), local('BeelineSans-Regular'), url(/static/fonts/BeelineSansV0.1-Regular.ttf);
            }
            `}
          </style>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </html>
    )
  }
}
