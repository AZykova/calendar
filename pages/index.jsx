import React from 'react';

const Home = () => (
  <style jsx global>{`
    html {
      font-family: 'BeelineSans';
    }

    @font-face {
      font-family: 'BeelineSans';
      font-weight: 400;
      font-style: normal;
      font-display: swap;
      src: local('BeelineSans'), local('BeelineSans-Regular'), url(/static/fonts/BeelineSansV0.1-Regular.ttf);
    }

     body {
      margin: 0;
      padding: 0;
     }
  `}
  </style>
);

export default Home;
