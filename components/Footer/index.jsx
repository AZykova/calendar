import React from 'react';

// Components
import { Logo } from 'components';

// Styles
import classNames from 'classnames/bind';
import styles from './styles.less';
const cx = classNames.bind(styles);

const Footer = () => (
  <div className={cx('footer')}>
    <ul>
      <li>
        <div>
          <Logo name="beeline" />
        </div>
      </li>
      {/*<li>*/}
        {/*<a href="https://beeinterns.ru/">*/}
          {/*<Logo name="beeInterns" />*/}
        {/*</a>*/}
      {/*</li>*/}
      {/*<li>*/}
        {/*<a href="https://www.youtube.com/channel/UC_mZP7jedeAK9bB3_CkoDjQ">*/}
          {/*<Logo name="frontendZavtrak" />*/}
        {/*</a>*/}
      {/*</li>*/}
    </ul>
  </div>
);

export default Footer;
