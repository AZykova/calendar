export const photos = [
  {
    src: "/static/1.png",
    width: 4,
    height: 3,
    date: 1,
    title: '1 февраля',
    weekend: false,
    description: ['Поучаствуйте в опросе по развитию программы BeeWINNER - обратная связь поможет команде сделать ее максимально интересной и полезной!']
  },
  {
    src: "/static/2.png",
    width: 3,
    height: 2,
    date: 2,
    title: '2 февраля',
    weekend: true,
    description: ['Ищешь единомышленников по киберспорту? Присоединяйся к сообществу в Билайн! Подавай заявку на участие в турнире по League of Legends​ до 7 февраля по ссылке <a target="_blank" href="https://space.beeline.ru/beewinner/Pages/league_of_legends_2021.aspx">перейти</a>'
    ]
  },
  {
    src: "/static/3.png",
    width: 4,
    height: 3,
    date: 3,
    title: '3 февраля',
    weekend: true,
    description: ['Встреча о том, как в наступившем году всё-таки сделать то, что хочется, уже прошла. Но мы готовим запись! Добавляйся в Telegram-канал BeeMIND, чтобы не пропустить ссылку. <a target="_blank" href="https://t.me/joinchat/VD1x9UrOE7byi45o">перейти</a>'
    ]
  },
  {
    src: "/static/4.png",
    width: 2,
    height: 2,
    date: 4,
    title: '4 февраля',
    weekend: true,
    description: ['Инвестиции — не только про высокие риски и миллионы на счетах. Вместе с финансистом и психологом Натальей Степановой разберем: · когда для сбережений достаточно депозита в банке, а когда пора воспользоваться финансовыми инструментами; · как устроена биржа, чем отличаются акции и облигации; · первые шаги, которые нужно сделать, когда вы приняли решение начать вкладывать деньги. Ссылка для регистрации <a target="_blank" href="https://events.webinar.ru/11308065/7946235">перейти</a>'
    ]
  },
  {
    src: "/static/5.png",
    width: 2,
    height: 3,
    date: 5,
    title: '5 февраля',
    weekend: true,
    description: ['На новой встрече клуба BeeFAMILY поговорим о проблема самоопределения подростков: · по каким признакам понять, что сын или дочь «созрели» для осознанного выбора? · стоит ли предлагать свои варианты и критиковать то, что нравится ребенку? · что делать, если подросток внезапно передумал и решил сменить ВУЗ перед сдачей ЕГЭ? Ссылка для регистрации <a target="_blank" href="https://events.webinar.ru/11308065/7946393">перейти</a>'
    ]
  },
  {
    src: "/static/7.png",
    width: 3,
    height: 4,
    date: 7,
    title: '7 февраля',
    weekend: true,
    description: ['Ищешь единомышленников по киберспорту? Присоединяйся к сообществу в Билайн! Подавай заявку на участие в турнире по League of Legends​ до 7 февраля по ссылке <a target="_blank" href="https://space.beeline.ru/beewinner/Pages/league_of_legends_2021.aspx">перейти</a>'
    ]
  },
  {
    src: "/static/6.png",
    width: 3,
    height: 2,
    date: 6,
    title: '6 февраля',
    weekend: true,
    description: ['Впереди много поводов для подарков себе и близким. Прежде чем отправиться на шопинг, загляните на платформу скидок для сотрудников SpotyGo. Возможно, вы сможете найти подходящее предложение и приятно сэкономить! <a target="_blank" href="https://beeline.spotygo.ru/">перейти</a>'
    ]
  },
  {
    src: "/static/8.png",
    width: 2,
    height: 2,
    date: 8,
    title: '8 февраля',
    weekend: true,
    description: ['Вебинары, челенджи и простые задания по эмоциональному интеллекту, личной трансформации и work life balance – всё, чтобы познакомиться с инструментом медитации и быть эффективным в любой, даже самой стрессовой ситуации. Регистрация на вводный вебинар по ссылке <a target="_blank" href="https://internal.beeline.ru/wheretogo/app/beesport/">перейти</a>'
    ]
  },
  {
    src: "/static/9.png",
    width: 2,
    height: 3,
    date: 9,
    title: '9 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/11.png",
    width: 2,
    height: 2,
    date: 11,
    title: '11 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/10.png",
    width: 2,
    height: 2,
    date: 10,
    title: '10 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/13.png",
    width: 1,
    height: 2,
    date: 13,
    title: '13 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/12.png",
    width: 4,
    height: 3,
    date: 12,
    title: '12 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/16.png",
    width: 3,
    height: 3,
    date: 16,
    title: '16 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/14.png",
    width: 4,
    height: 3,
    date: 14,
    title: '14 февраля',
    weekend: false,
    description: []
  },
  {
    src: "/static/15.png",
    width: 3,
    height: 2,
    date: 15,
    title: '15 февраля',
    weekend: true,
    description: ['Четыре недели вместе с профессиональными тренерами, которые помогут улучшить самочувствие и форму к весне. Чтобы принять участие, скачивайте приложение для занятий спортом BeeSPORT. Мы пришлем уведомление о старте марафона. <a target="_blank" href="http://onelink.to/pm8knf">перейти</a>'
    ]
  },
  {
    src: "/static/17.png",
    width: 4,
    height: 3,
    date: 17,
    title: '17 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/19.png",
    width: 3,
    height: 3,
    date: 19,
    title: '19 февраля',
    weekend: true,
    description: ['Призовая викторина на Space по киноитогам 2020. Подпишитесь на телеграм-канал Киноклуба, чтобы не пропустить начало! <a target="_blank" href="https://t.me/beecinema">перейти</a>'
    ]
  },
  {
    src: "/static/18.png",
    width: 3,
    height: 2,
    date: 18,
    title: '18 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/20.png",
    width: 2,
    height: 2,
    date: 20,
    title: '20 февраля',
    weekend: true,
    description: ['На встрече сообщества с корпоративным психологом поговорим про роль отца в воспитании ребенка. Ссылка для регистрации <a target="_blank" href="https://internal.beeline.ru/wheretogo/app/beefamily2002/">перейти</a>'
    ]
  },
  {
    src: "/static/21.png",
    width: 2,
    height: 3,
    date: 21,
    title: '21 февраля',
    weekend: true,
    description: ['После встречи по обещаниям пройдет ровно 21 день – говорят, этого времени достаточно, чтобы сформировать привычку. Мы снова встретимся с корпоративным психологом Еленой Кречко, чтобы свериться: как мы реализовываем обещания, данные самим себе? Регистрация по ссылке <a target="_blank" href="https://internal.beeline.ru/wheretogo/app/beemind2402/">перейти</a>'
    ]
  },
  {
    src: "/static/22.png",
    width: 2,
    height: 2,
    date: 22,
    title: '22 февраля',
    weekend: true,
    description: ['Если для вас актуальна тема, как восстановить ритм после школьных каникул – рекомендуем пересмотреть запись встречи с корпоративным психологом Еленой Кречко на тему «Ребенок и учеба» <a target="_blank" href="https://space.beeline.ru/News/Pages/2020/news_2ffbf0d95ef24ce99af91591c42d7cb0.aspx">перейти</a>'
    ]
  },
  {
    src: "/static/23.png",
    width: 2,
    height: 2,
    date: 23,
    title: '23 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/24.png",
    width: 4,
    height: 3,
    date: 24,
    title: '24 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/25.png",
    width: 4,
    height: 3,
    date: 25,
    title: '25 февраля',
    weekend: true,
    description: ['Регистрируйтесь на новую встречу экосообщества компании – мы обязательно пришлем все детали. Регистрация по ссылке <a target="_blank" href="https://internal.beeline.ru/wheretogo/app/beegreen2502/">перейти</a>'
    ]
  },
  {
    src: "/static/28.png",
    width: 2,
    height: 2.03,
    date: 28,
    title: '28 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/30.png",
    width: 2,
    height: 1.695,
    date: 30-31,
    title: '30-31 февраля',
    weekend: false,
    description: [
    ]
  },
  {
    src: "/static/29.png",
    width: 4,
    height: 3.39,
    date: 29,
    title: '29 февраля',
    weekend: false,
    description: []
  },
  {
    src: "/static/27.png",
    width: 4,
    height: 3.06,
    date: 27,
    title: '27 февраля',
    weekend: false,
    description: []
  },
  {
    src: "/static/26.png",
    width: 2,
    height: 1.2,
    date: 26,
    title: '26 февраля',
    weekend: true,
    description: ['Любители читать снова встретятся, чтобы обсудить хорошую книгу. Какую именно - решим в телеграм-чате сообщества. Присоединяйтесь! <a target="_blank" href="https://t.me/joinchat/FpvS0h0C-Pr5S95KkGaEAA">перейти</a>.']
  }
];
